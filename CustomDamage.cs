﻿/*Copyright (C) 2020 MacNokker (macnokker@gmail.com)

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, see <https://www.gnu.org/licenses>.

Additional permission under GNU GPL version 3 section 7

If you modify this library, or any covered work, by linking or combining it with "Taleworlds.Core", "TaleWorlds.MountAndBlade", "TaleWorlds.Library" and "Sandbox" (or modified versions of these libraries), containing parts covered by the terms of their applicable licenses, the licensors of this library grant you additional permission to convey the resulting work. 
*/

using HarmonyLib;
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;

namespace CustomDamage
{
    public class CustomDamage : MBSubModuleBase
	{

		private static void StatusFile()
		{
				string file = string.Concat(CDValues.Instance.Modfolder, "status.txt");
				string content = $"Status Report:{Environment.NewLine}";
				content += (CDValues.Instance.PresetLoads > 0) ? $"Preset Loaded: {CDValues.Instance.PresetName}{Environment.NewLine}": "";
				content += $"Config file read from: {CDValues.Instance.PathToConfig}{Environment.NewLine}";
				if (CDValues.Instance.DetectMats_Inited)
				{
					content += $"{Environment.NewLine}Material Detection: Config Ok{Environment.NewLine}";
					foreach (KeyValuePair <string, float> mat in CDValues.Instance.Material)
					{
						content += $"{mat.Key} {mat.Value}{Environment.NewLine}";
					}
				}
				else if (CDValues.Instance.DetectMats)
				{
					content += "Material Detection: Invalid Config" + Environment.NewLine;
				}
				if (CDValues.Instance.EffectVariety)
				{
					content += $"{Environment.NewLine}Effect Variety Enabled{Environment.NewLine}"
					+ "Attack Magnitude Settings:"+ Environment.NewLine;
					foreach (KeyValuePair <string, float> effect in CDValues.Instance.EffectMag)
					{
						content += $"{effect.Key} {effect.Value}{Environment.NewLine}";
					}
					content += "Damage Multipliers:" + Environment.NewLine;
					foreach (KeyValuePair <string, float> mult in CDValues.Instance.Dmgmult)
					{
						content += $"{mult.Key} {mult.Value}{Environment.NewLine}";
					}
					content += "Weapon Class Array Dump:";
					for (int i = 0; i < 31; i++)
					{
						content += $" {CDValues.Instance.WClass[i]}";
					}
				}
				File.WriteAllText(file,content);
		}

		private void StateMessage()
		{
			if (CDValues.Instance.PresetLoads > 0)
			{
			   if (CDValues.Instance.Runlevel == CDValues.Runlevels.preset_structureok && CDValues.Instance.MMMessage)
			   {
					InformationManager.DisplayMessage(new InformationMessage("Custom Damage Preset " + CDValues.Instance.PresetName + " Loaded"));     
			   }
			   else 
			   {
				   InformationManager.DisplayMessage(new InformationMessage(CDValues.Instance.Cfg_Error)); 
			   }
			}
			else if (CDValues.Instance.DetectMats_Inited && CDValues.Instance.MMMessage)
			{
				InformationManager.DisplayMessage(new InformationMessage("Custom Damage Material Detection: Active"));
			}
			else if (CDValues.Instance.DetectMats)
			{
				InformationManager.DisplayMessage(new InformationMessage("Custom Damage Material Detection Error" + CDValues.Instance.Cfg_Error));
				InformationManager.DisplayMessage(new InformationMessage("XML ValuesExpected 24, Encountered: " + CDValues.Instance.Material.Count));
			}
			else if (CDValues.Instance.AdvHitDetection && CDValues.Instance.MMMessage)
			{
				InformationManager.DisplayMessage(new InformationMessage("Custom Damage Adv Hit Detection: Active"));
			}            
			else if (CDValues.Instance.Runlevel == CDValues.Runlevels.running && CDValues.Instance.MMMessage)
			{
				string cv = "Custom Damage File Values: BFC:"
				+ CDValues.Instance.BaseVal[0]
				+ ", BFP:"
				+ CDValues.Instance.BaseVal[1]
				+ ", BFB:"
				+ CDValues.Instance.BaseVal[2]
				+ ", ArmorAbsorbFactor:"
				+ CDValues.Instance.BaseVal[3]
				+ ", ATFP:"
				+ CDValues.Instance.BaseVal[4]
				+ ", ATFC:"
				+ CDValues.Instance.BaseVal[5]
				+ ", ASDA:"
				+ CDValues.Instance.ArmorSkillDamageAbsorb;
				InformationManager.DisplayMessage(new InformationMessage(cv));
			}
			else
			{
				InformationManager.DisplayMessage(new InformationMessage("Custom Damage File NOT Loaded" + CDValues.Instance.Cfg_Error));
			}
		}

		protected override void OnBeforeInitialModuleScreenSetAsRoot()
		{
			if (CDValues.Instance.Printed)
			{
				return;
			}

			CDValues.Instance.Printed = true;
			StatusFile();

			var t = Task.Run(() => 
			{
				Random rng = new Random();
				Thread.Sleep(rng.Next(200,400));
				StateMessage();	
			});

		}

		protected override void OnSubModuleLoad()
		{
			base.OnSubModuleLoad();
			CDValues.Instance.Modfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/../../";
			CDValues.Instance.Modfolder = Path.GetFullPath(CDValues.Instance.Modfolder);
			string CFGPath = string.Concat(CDValues.Instance.Modfolder, "customdamage.xml");	
			

			if (!File.Exists(CFGPath))
			{
				string file = string.Concat(CDValues.Instance.Modfolder, "status.txt");
				File.WriteAllText(file,$"Could not find config file at: {CFGPath}");
				CDValues.Instance.Cfg_Error = CFGPath;
				return;
			}

				CDValues.Instance.Runlevel = CDValues.Runlevels.basefile_found;
				CDValues.Instance.CDInit(CFGPath);

				if (CDValues.Instance.Runlevel != CDValues.Runlevels.basefile_structureok)
					return;
				if (!CDValues.Instance.DetectMats_Inited && CDValues.Instance.DetectMats)
					return;

				try
				{
					var h = new Harmony("cia.bane.rlord.cdm");
					//Harmony.DEBUG = true;
					h.PatchAll();
					CDValues.Instance.Runlevel = CDValues.Runlevels.running;
				}
				catch (Exception exception)
				{
					string message;
					Exception innerException = exception.InnerException;
					if (innerException != null)
					{
						message = innerException.Message;
					}
					else
					{
						message = null;
					}
					Patch.CBDUtil.DebugFile(message);
				}



		}
	}
}
