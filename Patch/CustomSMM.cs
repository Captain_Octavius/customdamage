﻿/*Copyright (C) 2020 MacNokker (macnokker@gmail.com)

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, see <https://www.gnu.org/licenses>.

Additional permission under GNU GPL version 3 section 7

If you modify this library, or any covered work, by linking or combining it with "Taleworlds.Core", "TaleWorlds.MountAndBlade", "TaleWorlds.Library" and "Sandbox" (or modified versions of these libraries), containing parts covered by the terms of their applicable licenses, the licensors of this library grant you additional permission to convey the resulting work. 
*/
using System;
using System.IO;
using HarmonyLib;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;
using TaleWorlds.Library;
using SandBox;



namespace CustomDamage.Patch
{
	[HarmonyPatch(typeof(Agent))]
	class CustomAgent
	{

		[HarmonyPostfix]
		[HarmonyPriority(Priority.Last)]
		[HarmonyPatch("GetBaseArmorEffectivenessForBodyPart")]

		// determine hit location and armor material and store it in the return value
		static void GetBaseArmorEffectivenessForBodyPart(Agent __instance, ref float __result, BoneBodyPartType bodyPart)
		{
			if (!CDValues.Instance.AdvHitDetection || bodyPart == BoneBodyPartType.None) return;

			else if (__result > 255)
			{
				InformationManager.DisplayMessage(new InformationMessage("Armor Value Higher Than 255 Detected!"));
				InformationManager.DisplayMessage(new InformationMessage("Custom Damage is not designed to handle this!"));
				return;
			}

			EquipmentIndex equipmentIndex = EquipmentIndex.None;

			if (__instance.IsMount)
			{
				equipmentIndex = EquipmentIndex.HorseHarness;
			}
			else
			{
				switch (bodyPart)
				{
					case BoneBodyPartType.Head:
					case BoneBodyPartType.Neck:
						equipmentIndex = EquipmentIndex.NumAllWeaponSlots;
						break;
					case BoneBodyPartType.Chest:
					case BoneBodyPartType.Abdomen:
					case BoneBodyPartType.ShoulderLeft:
					case BoneBodyPartType.ShoulderRight:
						equipmentIndex = EquipmentIndex.Body;
						break;
					case BoneBodyPartType.BipedalArmLeft:
					case BoneBodyPartType.BipedalArmRight:
					case BoneBodyPartType.QuadrupedalArmLeft:
					case BoneBodyPartType.QuadrupedalArmRight:
						equipmentIndex = EquipmentIndex.Gloves;
						break;
					case BoneBodyPartType.BipedalLegs:
					case BoneBodyPartType.QuadrupedalLegs:
						equipmentIndex = EquipmentIndex.Leg;
						break;
				}
			}

			if (equipmentIndex == EquipmentIndex.None)
			{
				CBDUtil.DebugFile("Did Not Translate Body Part Type: " + bodyPart + " To EquipmentIndex");
				return;
			}

			ArmorComponent.ArmorMaterialTypes mat_type = ArmorComponent.ArmorMaterialTypes.None;

			if (__instance.SpawnEquipment[equipmentIndex].Item != null)
				mat_type = __instance.SpawnEquipment[equipmentIndex].Item.ArmorComponent.MaterialType;


			int type_num = (int)mat_type + 1;    // enum can be zero but I need to know this happened
			int bone_num = (int)bodyPart + 1;

			int mask_armor = 0xf << 8;       // mask 4 bits shifted 8 to the left; inverted later
			int mask_bone = 0xf << 12;       //		 4				12

			int armor = (int)__result;                                                      // temporarily convert to int

			int stored = (armor & ~mask_armor) | (type_num << 8);                        // write type_num into bits >  255
			stored = (stored & ~mask_bone) | (bone_num << 12);                       // write bone_num into bits > 4095

			__result = stored;                                                              // implicit convert back to float

			if (CDValues.Instance.DetectMats_Verbose)
				InformationManager.DisplayMessage(new InformationMessage("Armor Material Reported By Game As: " + mat_type));
		}
	}

	class CBDUtil
	{
		public enum cdsettings
		{
			BluntFactorCut,
			BluntFactorPierce,
			BluntFactorBlunt,
			ArmorAbsorbFactor,
			ArmorThresholdFactorPierce,
			ArmorThresholdFactorCut
		}

		public static void DebugFile(string msg)
		{
				string file = string.Concat(CDValues.Instance.Modfolder, "debug.txt");
				File.AppendAllText(file,msg + Environment.NewLine);
		}

		// get the applicable armor property from either the Material dictonary if in detect mat mode or get the value from the base array
		// todo: change from dictonary lookups to array index
		public static float ArmorConfigValue(cdsettings which, bool material_mode, ArmorComponent.ArmorMaterialTypes mat_type)
		{
			// if material detection was successful
			if (material_mode)

			{
				// build a dictonary key like "clotharmorabsorbfactor"
				string key = mat_type.ToString() + which.ToString();
				//InformationManager.DisplayMessage(new InformationMessage(key));

				// if corrs. value exist return it
				if (CDValues.Instance.Material.TryGetValue(key.ToLower(), out float value))
				{
					//InformationManager.DisplayMessage(new InformationMessage("Using Material Value :" + value));
					return value;
				}
			}
			//InformationManager.DisplayMessage(new InformationMessage("Using Base Value :" + (int)which));

			// fallback to base config value
			return CDValues.Instance.BaseVal[(int)which];			
		}


		// the TW damage formula as a method for use by the patch
		public static float ApplyArmorDamageReduction(DamageTypes damageType, float magnitude, int armorEffectiveness, float absorbedDamageRatio, bool material_mode, ArmorComponent.ArmorMaterialTypes mat_type)
		{

			if (armorEffectiveness == 0)
			{
				return magnitude * absorbedDamageRatio;
			}


			float bluntDamageFactorByDamageType = 0F;
			switch (damageType)
			{
				case DamageTypes.Cut:
					bluntDamageFactorByDamageType = ArmorConfigValue(cdsettings.BluntFactorCut, material_mode, mat_type);
					break;
				case DamageTypes.Pierce:
					bluntDamageFactorByDamageType = ArmorConfigValue(cdsettings.BluntFactorPierce, material_mode, mat_type);
					break;
				case DamageTypes.Blunt:
					bluntDamageFactorByDamageType = ArmorConfigValue(cdsettings.BluntFactorBlunt, material_mode, mat_type);		
					break;
			}

			

			float blunt = magnitude * bluntDamageFactorByDamageType;
			float armorabsorb = 100F / (100F + armorEffectiveness * ArmorConfigValue(cdsettings.ArmorAbsorbFactor, material_mode, mat_type));
			float damage = blunt * armorabsorb;
			if (damageType != DamageTypes.Blunt)
			{
				float nonblunt;
				if (damageType != DamageTypes.Cut)
				{
					if (damageType != DamageTypes.Pierce)
					{
						return 0F;
					}
					nonblunt = Math.Max(0f, magnitude * armorabsorb - armorEffectiveness * ArmorConfigValue(cdsettings.ArmorThresholdFactorPierce, material_mode, mat_type));
					nonblunt *= (1F - bluntDamageFactorByDamageType);
				}
				else
				{
					nonblunt = Math.Max(0f, magnitude * armorabsorb - armorEffectiveness * ArmorConfigValue(cdsettings.ArmorThresholdFactorCut, material_mode, mat_type));
					nonblunt *= (1F - bluntDamageFactorByDamageType);
				}
				
				damage += nonblunt;

			}
			return damage * absorbedDamageRatio;
		}

		// determine if the attack is melee or ranged, then apply the modifier from dictonary if found
		public static float ChangeMagnitude(DamageTypes damagetype, float magnitude, int weaponclass)
		{
			if (weaponclass < 0 || weaponclass > 30)
				return magnitude;
			

			// build dictonary key

			string key = (weaponclass < 12 ) ? "melee" : (weaponclass < 22) ? "ranged" : (weaponclass == 30) ? "melee" : "fail";

			key += damagetype.ToString();

			//CBDUtil.DebugFile("Attack is " + key + " SpeedBonus Lookup: " + CDValues.Instance.WClass[speedBonus]);
			magnitude *= CDValues.Instance.WClass[weaponclass];

			// if corrs. value exist return it	
			if (CDValues.Instance.EffectMag.TryGetValue(key.ToLower(), out float value))
			{
				//InformationManager.DisplayMessage(new InformationMessage(key + " Attack Magnitude Multiplier: " + value));
				return magnitude * value;
			}
			//CBDUtil.DebugFile("Value Not Found For: " + key);
			// fallback to input parameter
			return magnitude;
		}


		// get the applicable bone damage multiplier from the Dmgmult dictionary or return damageMultiplierOfBone if not found
		public static float GetDmgMult(float damageMultiplierOfBone, DamageTypes damageType, int bone_num)
		{
			// convert back to enum to get its string value and build key like "cuthead"
			BoneBodyPartType boneBodyPartType = (BoneBodyPartType)bone_num;
			string key = damageType.ToString() + boneBodyPartType.ToString();

			// CBDUtil.DebugFile(key + " lookup...");
			// if corrs. value exist return it
			if (CDValues.Instance.Dmgmult.TryGetValue(key.ToLower(), out float boneDamageFactor))
			{
				//InformationManager.DisplayMessage(new InformationMessage(key + " Damage Multiplier " + boneDamageFactor));
				// CBDUtil.DebugFile("ok. Changed Damage Multiplier To " + boneDamageFactor);
				return boneDamageFactor;
			}

			// CBDUtil.DebugFile("fail");

			// fallback to input parameter
			return damageMultiplierOfBone;
		}	

	}



	[HarmonyPatch(typeof(Mission))]
	class CustomCBD
	{

		[HarmonyPrefix]
		[HarmonyPriority(Priority.VeryHigh)]
		[HarmonyPatch("ComputeBlowDamage")]
		//static bool ComputeBlowDamage(float armorAmountFloat, WeaponComponentData shieldOnBack, AgentFlag victimAgentFlag, float victimAgentAbsorbedDamageRatio, float damageMultiplierOfBone, float combatDifficultyMultiplier, DamageTypes damageType, float magnitude, bool blockedWithShield, bool hitShieldOnBack, int speedBonus, bool isFallDamage, ref int inflictedDamage, ref int absorbedByArmor, ref int armorAmount)
		static bool ComputeBlowDamage(ref AttackInformation attackInformation, ref AttackCollisionData attackCollisionData, in MissionWeapon attackerWeapon, DamageTypes damageType, float magnitude, int speedBonus, bool cancelDamage, ref int inflictedDamage, ref int absorbedByArmor)
		{
			bool isHorse = (attackInformation.VictimAgentFlag & AgentFlag.Mountable) > AgentFlag.None;
			bool material_mode = false;
			ArmorComponent.ArmorMaterialTypes mat_type = ArmorComponent.ArmorMaterialTypes.None;
			int bone_num = 0;
			int armorAmount;

			if (!attackCollisionData.IsFallDamage)
			{
				armorAmount = (int)attackInformation.ArmorAmountFloat;								// permanently cast armor as int since the game never uses fractional armor values 


				if (CDValues.Instance.AdvHitDetection)							// in adv detection mode
				{
					if (armorAmount < 256)
					{
						//InformationManager.DisplayMessage(new InformationMessage("Detect Material Mode: Armor amount too small: " + armorAmount));
						//FileLog.Log("Detect Material Mode: Armor amount too small: " + armorAmount);
					}
					else															// if armorAmount is greater than 255
					{																// extract the previously stored values

						int mask_armor = 0xf << 8;                   // mask 4 bits shifted 8 to the left;
						int mask_bone = 0xf << 12;
						int mask_reset = ~mask_armor & ~mask_bone;

						int type_num = (armorAmount & mask_armor) >> 8;
						bone_num = (armorAmount & mask_bone) >> 12;

						armorAmount &= mask_reset;

						if (type_num < 1 || type_num > 5)
						{
							//InformationManager.DisplayMessage(new InformationMessage("Detect Material Mode: Not a valid material code : " + value));
							//FileLog.Log("Detect Material Mode: Not a valid material code : " + value);
						}
						else if (CDValues.Instance.DetectMats_Inited)						// in Detect Material Mode
						{
							material_mode = true;
							type_num--;
							mat_type = (ArmorComponent.ArmorMaterialTypes)type_num;
							//InformationManager.DisplayMessage(new InformationMessage("Detect Material Mode: Material: " + mat_type + " Armor amount: "+ armorAmount));
							//InformationManager.DisplayMessage(new InformationMessage("Bone Hit: " + (bone_num -1)));
						}

					}
				}

				if (isHorse && CDValues.Instance.ToddHoward != 1F)				// It just works!
				{
					armorAmount = (int)(armorAmount * CDValues.Instance.ToddHoward);
				}


			}
			else
			{
				armorAmount = 0;
			}
			if (attackCollisionData.CollidedWithShieldOnBack && attackInformation.ShieldOnBack != null)
			{
				armorAmount += 10;
			}

			// assign new Damage Type for Bows if set
			DamageTypes temp_dType = (CDValues.Instance.Change_Arrow && attackerWeapon.CurrentUsageItem.WeaponClass == WeaponClass.Bow) ? CDValues.Instance.DType_Arrow : damageType;

			// CBDUtil.DebugFile("Damage Type in " +damageType + "Damage Type out "+ temp_dType);

			if (CDValues.Instance.EffectVariety)
			{
				// change attack magnitude
				// CBDUtil.DebugFile("Calling ChangeMagnitude with  " + speedBonus);
				magnitude = CBDUtil.ChangeMagnitude(damageType, magnitude, (int)attackerWeapon.CurrentUsageItem.WeaponClass);

				if (bone_num < 1 || bone_num > 12)
				{
					CBDUtil.DebugFile("Bone Index out of bounds: " + bone_num);					
				}
				else
				{
					bone_num--;
					// get new damage multiplier (applied later)
					//CBDUtil.DebugFile("Calling GetDMGMult for bone " + bone_num);
					attackInformation.DamageMultiplierOfBone = CBDUtil.GetDmgMult(attackInformation.DamageMultiplierOfBone, temp_dType, bone_num);
				}	
			}

			float damage_reduced = CBDUtil.ApplyArmorDamageReduction(damageType, magnitude, armorAmount, attackInformation.VictimAgentAbsorbedDamageRatio, material_mode, mat_type);

			float damagenoarmor = (magnitude * attackInformation.VictimAgentAbsorbedDamageRatio);					// calculate damagenoarmor without redundant function call

			absorbedByArmor = (int)(damagenoarmor - damage_reduced);							// determine the amt of dmg absorbed
			absorbedByArmor = MBMath.ClampInt(absorbedByArmor, 0, 2000);

			//FileLog.Log("DNo Armor " + damagenoarmor + " DReduced " + damage_reduced + " Absorbed " + absorbedByArmor);

			float damagemult = 1F;
			if (!attackCollisionData.AttackBlockedWithShield && !attackCollisionData.IsFallDamage)
			{
				if (isHorse)
				{
					if (CDValues.Instance.Dmgmult.TryGetValue(temp_dType.ToString().ToLower() + "horse", out float horsemult))
					{
						damagemult *= horsemult;
						//InformationManager.DisplayMessage(new InformationMessage("Horse Mult Applied " + temp_dType));
					}
				}

				damagemult *= attackInformation.DamageMultiplierOfBone;
				damagemult *= attackInformation.CombatDifficultyMultiplier;
			}

			damage_reduced *= damagemult;													// apply damage mults after absorbedByArmor
			inflictedDamage = MBMath.ClampInt((int)damage_reduced, 0, 2000);

			//FileLog.Log("BoneMult " + damageMultiplierOfBone + " Difficulty " + combatDifficultyMultiplier + " FullDMG " + inflictedDamage);

			return false;
		}
		



/*		[HarmonyPostfix]
		[HarmonyPriority(Priority.Low)]
		[HarmonyPatch("ComputeBlowMagnitudeImp")]
		static void ComputeBlowMagnitudeImp(ref AttackCollisionData acd, ref int speedBonusInt, WeaponComponentData weapon)
		{

			if (acd.IsMissile)
			{
				speedBonusInt = (int)weapon.WeaponClass;
				// FileLog.Log("Item " + weapon.Item.GetName() + "Class " + weapon.WeaponClass);
				return;
			}
			if (acd.IsFallDamage)
			{
				return;
			}
			if (acd.IsHorseCharge)
			{
				return;
			}
				if (weapon != null)
				{
					speedBonusInt = (int)weapon.WeaponClass;
					// FileLog.Log("Item " + weapon.Item.GetName() + "Class " + weapon.WeaponClass);
					//InformationManager.DisplayMessage(new InformationMessage("Item " + weapon.Item.GetName() + "Class " + weapon.WeaponClass));
					return;
				}
				speedBonusInt = 30;	//reuse of an unused variable: 30 - melee unarmed

		}*/


		[HarmonyPostfix]
		[HarmonyPriority(Priority.Low)]
		[HarmonyPatch("GetDamageMultiplierOfCombatDifficulty")]
		static void GetDamageMultiplierOfCombatDifficulty(Agent affectedAgent, ref float __result)
		{
			if (CDValues.Instance.DmgMultHero == 1F)								// create a hero damage mult if requested
			{
				return;
			}

			if (affectedAgent.RiderAgent != null)								// isHorse?
			{
		
				if (!affectedAgent.RiderAgent.IsMainAgent && affectedAgent.RiderAgent.IsHero)		// is rider a hero but not player
				{
					//InformationManager.DisplayMessage(new InformationMessage("Rider is Non Player Hero!"));
					__result = CDValues.Instance.DmgMultHero;
				}
				return;
			}
			else if (!affectedAgent.IsHuman)
			{
				return;
			}
			if (affectedAgent.IsMainAgent)
			{
				return;
			}
			if (affectedAgent.IsHero) 											// if hero, not player apply damagemult
			{
				//InformationManager.DisplayMessage(new InformationMessage("Non Player Hero!"));
				__result = CDValues.Instance.DmgMultHero;
			}
		}
	}

	[HarmonyPatch(typeof(SandboxAgentApplyDamageModel))]
	class CustomSBD
	{
		[HarmonyPostfix]
		[HarmonyPriority(Priority.Low)]
		[HarmonyPatch("CalculateDamage")]									// ArmorSkillDamageAbsorb implementation
		static void CalculateDamage(ref float __result, ref AttackCollisionData collisionData)
		{
			if (CDValues.Instance.ArmorSkillDamageAbsorb <= 0)
				return;

			//InformationManager.DisplayMessage(new InformationMessage("IFD " + collisionData.InflictedDamage + " Result " + __result));

			float skilled_damage = __result - collisionData.InflictedDamage;
			//InformationManager.DisplayMessage(new InformationMessage("Skill Damage Detected: " + skilled_damage));

			// return if no (skill) damage received
			if (skilled_damage <= 0 || collisionData.InflictedDamage <= 0)
			{
				//InformationManager.DisplayMessage(new InformationMessage("Factor " + absorbfactor + " No SkillDamage Absorbed"));
				return;
			}

			//InformationManager.DisplayMessage(new InformationMessage("Skill Damage Detected: " + skilled_damage));

			float absorbed = Math.Min(collisionData.AbsorbedByArmor, collisionData.InflictedDamage);			// determine the victims overall damage absorbtion %
			float absorbfactor = absorbed / collisionData.InflictedDamage;							// for this hit
			absorbfactor = Math.Min(absorbfactor * CDValues.Instance.ArmorSkillDamageAbsorb, 1F);			// cap it at 100%

			float skilled_absorb = (skilled_damage * absorbfactor);										// determine how much is to be absorbed as int

			if (skilled_absorb > 0)																			// if greater 0 apply 
			{
				//InformationManager.DisplayMessage(new InformationMessage("Factor " + absorbfactor + " SkillDamage Absorbed" + skilled_absorb));
				__result -= skilled_absorb;
			}
		}
	}

}