/*Copyright (C) 2020 MacNokker (macnokker@gmail.com)

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, see <https://www.gnu.org/licenses>.

Additional permission under GNU GPL version 3 section 7

If you modify this library, or any covered work, by linking or combining it with "Taleworlds.Core", "TaleWorlds.MountAndBlade", "TaleWorlds.Library" and "Sandbox" (or modified versions of these libraries), containing parts covered by the terms of their applicable licenses, the licensors of this library grant you additional permission to convey the resulting work. 
*/

using System;
using System.IO;
using System.Globalization;
using System.Xml;
using System.Collections.Generic;
using TaleWorlds.Core;


namespace CustomDamage
{
    public class CDValues
	{
		public string Modfolder { get; internal set; } = "";
		public string PathToConfig { get; private set; } = ""; 
		public int PresetLoads { get; private set;} = 0;
		public string PresetName {get; private set;} = "CDDefault";
		public float[] BaseVal { get; private set; } = new float[6];

		public float ArmorSkillDamageAbsorb { get; private set; } = 0F;
		public float ToddHoward { get; private set; } = 1F;
		public float DmgMultHero { get; private set; } = 1F;

		public bool AdvHitDetection { get; private set; }   = false;
		public bool DetectMats { get; private set; }    = false;
		public bool DetectMats_Verbose { get; private set; }    = false;
		public bool DetectMats_Inited { get; private set; } = false;

		public Dictionary<string, float> Material = new Dictionary<string, float> { };


		public bool EffectVariety { get; private set; } = false;

		public bool MMMessage { get; private set; } = true;
		public bool Printed { get; internal set; } = false;
		

		public Dictionary<string, float> EffectMag = new Dictionary<string, float> { };

		public Dictionary<string, float> Dmgmult = new Dictionary<string, float> { };

		public float[] WClass { get; private set; }

		public bool Change_Arrow { get; private set; } = false;
		public DamageTypes DType_Arrow { get; private set; } = DamageTypes.Pierce;

		public string Cfg_Error { get; internal set;  } = "";
		public Runlevels Runlevel { get; internal set;} = Runlevels.init;
		public enum Runlevels
		{
			init,
			basefile_found,
			basefile_check,
			basefile_structureok,
			running,
			preset_load,
			preset_structureok,
		}

		void dictwrite(Dictionary<string, float> dict, string key, float value)
		{
			if (!dict.ContainsKey(key))
			{
				dict.Add(key, value);
			}
		}

		void flushconfig()
		{
			Material.Clear();
			EffectMag.Clear();
			Dmgmult.Clear();
			BaseVal = new float[6];
			ArmorSkillDamageAbsorb = 0F;
			ToddHoward = 1F;
			DmgMultHero = 1F;
			AdvHitDetection = false;
			DetectMats = false;
			DetectMats_Verbose = false;
			DetectMats_Inited = false;
			EffectVariety = false;
			MMMessage = true;
			Change_Arrow = false;
			DType_Arrow = DamageTypes.Pierce;
			WClass = new float[31];
			for (int i = 0; i < 31; i++)
			{
				WClass[i] = 1F;
			}
		}

		// Parse the given config file and build an error string
		public string CDInit(string CFGPath = "")
		{
			//FileLog.Log("Phase 1a " + Instance.Runlevel);
			switch (Instance.Runlevel)
			{
				case Runlevels.basefile_found:

					Instance.Runlevel = Runlevels.basefile_check;
					break;

				case Runlevels.running:

					if (string.IsNullOrWhiteSpace(CFGPath))
					{
						return "No File Path Received";
					}
					Instance.Runlevel = Runlevels.preset_load;
					break;

				default:
 
					if (Instance.Runlevel != Runlevels.running)
					{
						return "Custom Damage is not running";
					}
					return $"Cannot load file at runlevel {Instance.Runlevel}";

			}
	
			//FileLog.Log("Phase 1b "+Instance.Runlevel);

			Cfg_Error = "No Error";
			XmlDocument xd = new XmlDocument();
			xd.Load(CFGPath);

			XmlNode node = xd.SelectSingleNode("/config/base");

			if (node == null)
			{
				Cfg_Error = ": Base Config Node Not Found";
				return "No Base Config Node in preset";				
			}
				//FileLog.Log("Phase 2a "+Instance.Runlevel);
				switch (Instance.Runlevel)
				{
					case Runlevels.basefile_check:

						CDValues.Instance.Runlevel = CDValues.Runlevels.basefile_structureok;
						break;

					case Runlevels.preset_load:

						PresetLoads++;
						flushconfig();
						Instance.Runlevel = Runlevels.preset_structureok;
						break;

					default:
						Cfg_Error = ": Runlevel fail at CDInit stage 2";
						return "Runlevel fail at CDInit stage 2";

				}

				PathToConfig = Path.GetFullPath(CFGPath);

				//FileLog.Log("Phase 2b "+Instance.Runlevel);
				foreach (XmlNode child_node in node.ChildNodes)
				{

					if (!child_node.HasChildNodes)
					{
						continue;
					}
					if (child_node.FirstChild.NodeType != XmlNodeType.Text || child_node.ChildNodes.Count != 1)
					{
						continue;
					}

					switch (child_node.Name.Trim().ToLower())
					{

						case "presetname":
							PresetName = child_node.FirstChild.Value;
							break;
						case "bluntfactorcut":
							BaseVal[0] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "bluntfactorpierce":
							BaseVal[1] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "bluntfactorblunt":
							BaseVal[2] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "armorabsorbfactor":
							BaseVal[3] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "armorthresholdfactorpierce":
							BaseVal[4] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "armorthresholdfactorcut":
							BaseVal[5] = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "armorskilldamageabsorb":
							ArmorSkillDamageAbsorb = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "toddhoward":
							ToddHoward = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "detectmats":
							DetectMats = bool.Parse(child_node.FirstChild.Value);
							break;
						case "dtmverbose":
							DetectMats_Verbose = bool.Parse(child_node.FirstChild.Value);
							break;
						case "effectvariety":
							EffectVariety = bool.Parse(child_node.FirstChild.Value);
							break;
						case "dmgmult_hero":
							DmgMultHero = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
							break;
						case "mmmessage":
							MMMessage = bool.Parse(child_node.FirstChild.Value);
							break;
					}
				}
			
			if (EffectVariety)
			{
				// needs AdvHitDetection
				AdvHitDetection = true;
				

				WClass = new float[31];
				for ( int i = 0; i < 31; i++ )
				{
					WClass[i] = 1F;
				}

				XmlNode effects_node = xd.SelectSingleNode("/config/effects/dmgmult");

				if (effects_node == null)
				{
					Cfg_Error = ": effects/dmgmult Node Not Found";
				}
				else
				{
					try
					{
						foreach (XmlNode effect_type_node in effects_node.ChildNodes)
						{
							string effect_name = effect_type_node.Name.Trim().ToLower();
							if (effect_name == "dtype_arrow")
							{
								if (!effect_type_node.HasChildNodes)
								{
									continue;
								}
								if (effect_type_node.FirstChild.NodeType != XmlNodeType.Text || effect_type_node.ChildNodes.Count != 1)
								{
									continue;
								}
								if (effect_type_node.FirstChild.Value != "default")
								{
									try
									{
										DType_Arrow = (DamageTypes)Enum.Parse(typeof(DamageTypes),effect_type_node.FirstChild.Value, true);
										Change_Arrow = true;
									}
									catch (System.Exception)
									{
										Change_Arrow = false;
									}
								}							
								continue;							
							}


							foreach (XmlNode bone_node in effect_type_node.ChildNodes)
							{
								if (!bone_node.HasChildNodes)
								{
									continue;
								}
								if (bone_node.FirstChild.NodeType != XmlNodeType.Text || bone_node.ChildNodes.Count != 1)
								{
									continue;
								}

								float node_value = float.Parse(bone_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);

								string bone_name = bone_node.Name.Trim().ToLower();
								string node_name;

								switch (bone_name)
								{
									case "head":
									case "neck":
									case "chest":
									case "abdomen":
										node_name = effect_name + bone_name;
										dictwrite(Dmgmult, node_name, node_value);
										break;
									case "shoulders":
										node_name = effect_name + "shoulderleft";
										dictwrite(Dmgmult, node_name, node_value);
										node_name = effect_name + "shoulderright";
										dictwrite(Dmgmult, node_name, node_value);
										break;
									case "arms":
										node_name = effect_name + "bipedalarmleft";
										dictwrite(Dmgmult, node_name, node_value);
										node_name = effect_name + "bipedalarmright";
										dictwrite(Dmgmult, node_name, node_value);
										node_name = effect_name + "quadrupedalarmleft";
										dictwrite(Dmgmult, node_name, node_value);
										node_name = effect_name + "quadrupedalarmright";
										dictwrite(Dmgmult, node_name, node_value);
										break;
									case "legs":
										node_name = effect_name + "bipedallegs";
										dictwrite(Dmgmult, node_name, node_value);
										break;
									case "legs_horse":
										node_name = effect_name + "quadrupedallegs";
										dictwrite(Dmgmult, node_name, node_value);
										break;
									case "horse":
										dictwrite(Dmgmult, effect_name + "horse", node_value);
										break;
								}

							}
						}

					}
					finally
					{
					}

					XmlNode mf_node = xd.SelectSingleNode("/config/effects/magnitude_factors");

					if (mf_node == null)
					{
						Cfg_Error = ": effects/magnitude_factors Node Not Found";
					}
					else
					{
						try
						{
							foreach (XmlNode combat_type_node in mf_node.ChildNodes)
							{
								string ctype_name = combat_type_node.Name.Trim().ToLower();

								if (ctype_name == "wclass")
								{
									foreach (XmlNode wclass_node in combat_type_node.ChildNodes)
									{
										if (!wclass_node.HasChildNodes)
										{
											continue;
										}
										if (wclass_node.FirstChild.NodeType != XmlNodeType.Text || wclass_node.ChildNodes.Count != 1)
										{
											continue;
										}
										
										try
										{
											WeaponClass weaponClass = (WeaponClass)Enum.Parse(typeof(WeaponClass),wclass_node.Name, true);
											WClass[(int)weaponClass] = float.Parse(wclass_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
										}
										catch (System.Exception)
										{
											if (wclass_node.Name.ToLower() == "unarmed")
												WClass[30] = float.Parse(wclass_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
										}
									}
								}
								else
								{

									foreach (XmlNode damage_type_value in combat_type_node.ChildNodes)
									{
										if (!damage_type_value.HasChildNodes)
										{
											continue;
										}
										if (damage_type_value.FirstChild.NodeType != XmlNodeType.Text || damage_type_value.ChildNodes.Count != 1)
										{
											continue;
										}

										float key_value = float.Parse(damage_type_value.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);

										string key_name = ctype_name + damage_type_value.Name.Trim().ToLower();

										dictwrite(EffectMag, key_name, key_value);

									}

								}
								

							}

						}
						finally
						{							
						}
					}
				}
			}

			if (DetectMats)
			{
				// needs AdvHitDetection
				AdvHitDetection = true;
				
				XmlNode material_node = xd.SelectSingleNode("/config/materials");

				if (material_node == null)
				{
					Cfg_Error = ": materials Node Not Found";
				}
				else
				{
					try
					{
						foreach (XmlNode mat_type_node in material_node.ChildNodes)
						{
							string mat_name = mat_type_node.Name.Trim().ToLower();
							foreach (XmlNode mat_type_value in mat_type_node.ChildNodes)
							{
								string node_name = mat_name + mat_type_value.Name.Trim().ToLower();
								if (!mat_type_value.HasChildNodes)
								{
									continue;
								}
								if (mat_type_value.FirstChild.NodeType != XmlNodeType.Text || mat_type_value.ChildNodes.Count != 1)
								{
									continue;
								}

								float node_value = float.Parse(mat_type_value.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);

								dictwrite(Material, node_name, node_value);

							}
						}

					}
					finally
					{
						DetectMats_Inited = (Material.Count == 24);
					}
				}
			}

			if (PresetLoads > 0)
			{
				Cfg_Error = $"{PresetName} Loading Error: {Cfg_Error}";
			}
			return Cfg_Error;
		}

        private static CDValues _instance = null;
		public static CDValues Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new CDValues();
				}
				return _instance;
			}
		}


	}
}
